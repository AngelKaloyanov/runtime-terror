package com.telerikacademy.core.strings;

@SuppressWarnings("StringConcatenationInLoop")
public class StringHelpers {
    /**
     * Abbreviates a string using ellipses.
     *
     * @param source String - *The string to modify*
     * @param maxLength int - *Maximum length of the resulting string*
     *
     * @return `String` - *The abbreviated String*
     *
     * @example abbreviate(" Telerik Academy ", 13)
     *            //returns: Telerik Acade...
     *
     * @author Deyan Stanchev
     */

    public static String abbreviate(String source, int maxLength) {
        String abbreviation = "";
        if (source.length() == 0) {
            return "";
        } else if (maxLength > source.length()) {
            return source;
        }
        for (int i = 0; i < maxLength; i++) {
            abbreviation += source.charAt(i);
        }
        abbreviation += "...";
        return abbreviation;
    }
    /**
     * Capitalizes a string changing the first character to title case.
     *
     * @param source String - The string to modify first letter to title case.
     * @return String - The capitalized string or empty string if source is empty.
     * @author Kristian Kinchev
     */
    public static String capitalize(String source) {
        if (source.length() == 0) {
            return "";

        }
        char[] array = source.toCharArray();
        array[0] = Character.toUpperCase(array[0]);
        return new String(array);

    }

    /**
     * Concatenates two Strings
     *
     * @param string1 is the first string
     * @param string2 is the second string, to be added to the end of the first string
     * @return concatenated String and returns only string2 if string1 is empty
     * @author Nadya Chukleva
     */
    public static String concat(String string1, String string2) {
        String concatenatedStrings = string1 + string2;
        return concatenatedStrings;
    }

    /**
     * Checks if string contains a symbol.
     *
     * @param source String- The string to check.
     * @param symbol char- The character to check for in a string.
     * @return boolean - true if symbol is within source or false if not.
     * @author Kristian Kinchev
     */
    public static boolean contains(String source, char symbol) {
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbol) {
                return true;
            }
        }
        return false;
    }
    /**
     * Checks if the string <code>source</code> ends with the given character.
     *
     * @param source  String - The string to check.
     * @param target char - The character to check for.
     * @return <code>boolean </code> - <code>true</code> if the string ends with <code>target</code>, else <code>false</code>.
     * @author Angel Gechev
     */
    public static boolean endsWith(String source, char target) {
        if (source.length() > 0) {
            char last = source.charAt((source.length() - 1));
            if (last == target){
                return true;
            } else {
                return false;
            }
        } else return false;
    }


    /**
     *Finds the first index of `target` within `source`.
     *
     * ### Parameters
     *
     * @param source String - The string to check
     *
     * @param target char - The character to check for
     *
     * @return int - The first index of target within source, otherwise, -1
     *
     * @example firstIndexOf("Java", 'a')
     * //returns: 1
     * @author Angel Kaloyanov
     *  */


    public static int firstIndexOf(String source, char target) {
        int sourceLength = source.length();
        String [] numbersAsStrings = new String [sourceLength];
        int targetAcquiredIndex = -1;

        for (int i = 0; i < sourceLength; i++){
            if (source.charAt(i) == target){
                targetAcquiredIndex = i;
                break;
            }
            else {
                targetAcquiredIndex = -1;
            }
        }

        return targetAcquiredIndex;
    }



    /**
     * Finds the last index of `target` within `source`.
     *
     * @param source String - *The string to check*
     * @param symbol - *The character to check for*
     *
     * @return `int` - *The last index `symbol` within `source` or -1 if no match*
     *
     * @example lastIndexOf("Java", 'a')
     *          //returns:  3
     *
     * @author Deyan Stanchev
     */
    public static int lastIndexOf(String source, char symbol) {
        int lastIndex = -2;
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbol) {
                lastIndex = i;
            }
        }
        if (lastIndex == -2) {
            return -1;
        }
        return lastIndex;
    }
    /**
     * Pads string on the left and right sides if it's shorter than length..
     *
     * @param source String - The string to pad.
     * @param length int - The length of the string to achieve.
     * @param paddingSymbol char-  The character used as padding  .
     * @return String - The padded string.
     * @author Kristian Kinchev
     */
    public static String pad(String source, int length, char paddingSymbol) {
        if (source.length() < length) {
            int left = (length - source.length()) / 2;
            int right = (length - source.length()) / 2;
            String padString = "";
            for (int i = 0; i < left; i++) {
                padString += paddingSymbol;
            }
            padString += source;
            for (int i = 0; i < right; i++) {
                padString += paddingSymbol;
            }
            return padString;
        } else {
            return source;
        }


    }
    /**
     * Pads symbol on the right side of the String to achieve the desired string length,
     * when desired length is longer than the initial String
     *
     * @param source String - The string to pad
     * @param length int - The length of the string to achieve
     * @param paddingSymbol char - The character used as padding
     * @return String - The padded string
     * @author Nadya Chukleva
     */
    public static String padEnd(String source, int length, char paddingSymbol) {
        int indexDifference = length - source.length();
        for (int index = 0; index < indexDifference; index++) {
            source = source + paddingSymbol;
        }
        return source;
    }

    /**
     * Pads source on the left side with paddingSymbol enough times to reach desired length
     *
     * @param source String - The string to pad
     * @param length int - The length of the string to achieve
     * @param paddingSymbol char - The character used as padding
     * @return String - The padded string
     * @example padStart("Java", 6, '*');
     * //returns: **Java
     * @author Nelly Jekova
     */

    public static String padStart(String source, int length, char paddingSymbol) {
        int difference = length - source.length();
        for (int i = 0; i < difference; i++) {
            source = paddingSymbol + source;
        }

        return source;
    }

    /**
     * Repeats the given string <code>times</code>> times.
     *
     * @param source String - The string to repeat.
     * @param times int - The number of times to repeat the string.
     * @return <code>String</code> The repeated string.
     * @author Angel Gechev
     */
    public static String repeat(String source, int times) {
        if (times == 0){
            return source;
        } else {
            String output = "";
            for (int i = 0; i < times; i++) {
                output += source;
            }
            return output;
        }
    }

    /**
     * Reverses `source` so that the first element becomes the last, the second element becomes the second to last, and so on.
     *
     * @param source String - The string to reverse
     *
     * @return String - The reversed string
     *
     * @example reverse("Java");
     * //returns: avaJ
     *
     * @author Angel Kaloyanov
     */

    public static String reverse(String source) {
        String input = source;
        StringBuilder reverseStringBuilder = new StringBuilder();
        reverseStringBuilder.append(input);
        reverseStringBuilder.reverse();
        String reverseString = String.valueOf(reverseStringBuilder);


        return reverseString;
    }



    /**
     * Returns a new string, starting from `start` and ending at `end`.
     *
     * @param source String - *The source String*
     * @param start int - *The starting position in `source` (inclusive)*
     * @param end int - *The end position in `source` (inclusive)*
     *
     * @return `String` - *A new string, formed by the characters in `source`, starting from `start` to `end`*
     *
     * @example section("**Telerik Academy**", 2, 16);
     *          //returns: Telerik Academy
     *
     * @author Deyan Stanchev
     */
    public static String section(String source, int start, int end) {
        String sub = "";
        for (int i = start; i <= end; i++) {
            sub += source.charAt(i);
        }
        return sub;
    }
    /**
     * Checks if the string starts with the given character.
     *
     * @param source String - The string to inspect.
     * @param target char - The character to search for.
     * @return boolean - true if string starts with target, otherwise false.
     * @author Kristian Kinchev
     */
    public static boolean startsWith(String source, char target) {
        if (source.length() > 0 && source.charAt(0) == target) {
            return  true;
        }
        return false;

    }
}

