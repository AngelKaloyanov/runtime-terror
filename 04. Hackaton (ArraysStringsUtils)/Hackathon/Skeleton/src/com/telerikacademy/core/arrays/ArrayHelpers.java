package com.telerikacademy.core.arrays;

@SuppressWarnings({"ManualArrayCopy", "ExplicitArrayFilling"})
public class ArrayHelpers {

    /**
     * Adds a new <code>element</code> at the end of the <code>source array</code>.
     *
     * @param source  int[] - The array to amend.
     * @param element int - The element to add at the end.
     * @return <code>int[]</code> - A new <code>array</code>, the original array with the new <code>element</code> at the end.
     * @author Angel Gechev
     */
    public static int[] add(int[] source, int element) {
        int[] output = new int[source.length + 1];
        for (int i = 0; i < source.length; i++) {
            output[i] = source[i];
        }
        output[output.length - 1] = element;
        return output;
    }

    /**
     * Adds `element` at the start of `source`.
     *
     * @param source int[] - The array to add to.
     * @param element int - *The element to add*
     * @return int[] - *A new array, the original array with `element` at head position*
     *
     * @example add(new int[] { 1, 2, 3 }, 4);
     *          //returns {4, 1, 2, 3}
     * @author Deyan Stanchev
     */

    public static int[] addFirst(int[] source, int element) {
        int[] array = new int[source.length + 1];
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                array[i] = element;
            } else {
                array[i] = source[i - 1];
            }
        }
        return array;
    }

    /**
     *Adds all `elements` to the end of `source`.
     *
     * @param source int[] - The array to add to
     * @param elements int - The elements to add
     * @return int[] - A new array, the original array with all `elements` at the end
     * @example java
     * addAll(new int[] {1, 2, 3}, 4, 5, 6);
     * //returns: {1, 2, 3, 4, 5, 6}
     * @author Angel Kaloyanov
     */

    public static int[] addAll(int[] source, int... elements) {

        int additionalElementsCounter = 0;
        for (int element : elements) {
            additionalElementsCounter++;
        }
        int LengthOfNewArray = source.length + additionalElementsCounter;

        int[] arrAddAll = new int[LengthOfNewArray];
        for (int i = 0; i < source.length; i++) {
            arrAddAll[i] = source [i];
        }
        for (int i = source.length; i < LengthOfNewArray; i++){
            arrAddAll[i] = elements[i-source.length];
        }
        return arrAddAll;

    }


    /**
     * Checks if source contains element
     *
     * @param source  int [] - The array to check in
     * @param element int - The element to check for
     * @return boolean - true if source contains elements, otherwise, false
     * @example contains(new int[]{1, 2, 3}, 2);
     * //returns: true
     * @author Nelly Jekova
     */

    public static boolean contains(int[] source, int element) {
        for (int i = 0; i < source.length; i++) {
            if (source[i] == element) {
                return true;
            }
        }
        return false;
    }

    /**
     *Adds element from one array to another array and filling the remaining indexes, if any,
     * of the second array with default values
     *
     * @param sourceArray - the array to copy from
     * @param destinationArray - the array to copy to
     * @param count - count of indexes to be copied
     * @author - Nadya Chukleva
     */
    public static void copy(int[] sourceArray, int[] destinationArray, int count) {
        for (int index = 0; index < count; index++) {
            if (index < sourceArray.length) {
                destinationArray[index] = sourceArray[index];
            }
        }
    }

    /**
     * Copies elements from sourceArray, starting from sourceStartIndex into destinationArray, starting from destStartIndex, taking count elements.
     *
     * @param sourceArray      int [] - The array to copy from
     * @param sourceStartIndex int [] - The starting index in sourceArray
     * @param destinationArray int [] - The array to copy to
     * @param destStartIndex   int [] - The starting index in destinationArray
     * @param count            int - The number of elements to copy
     * @example int[] array = {1, 2, 3, 4, 5};
     * sourceStartIndex = 0;
     * int[] destinationArray = new int[4];
     * destStartIndex = 1;
     * count = 2;
     * copyFrom(array, sourceStartIndex, destinationArray, destStartIndex, count);
     * //destinationArray == {0, 1, 2, 0}
     * @author Nelly Jekova
     */

    public static void copyFrom(int[] sourceArray, int sourceStartIndex,
                                int[] destinationArray, int destStartIndex, int count) {
        for (int i = 0; i < count; i++) {
            destinationArray[destStartIndex] = sourceArray[sourceStartIndex];
            destStartIndex++;
            sourceStartIndex++;
        }

    }

    /**
     * Fills <code>source</code> with <code>element</code>.
     *
     * @param source  int[] - The array to fill
     * @param element int - The element to fill with
     * @return <code>void</code>>
     * @author Angel Gechev
     */
    public static void fill(int[] source, int element) {
        for (int i = 0; i < source.length; i++) {
            source[i] = element;
        }

    }

    /**
     * Finds the first index of `target` within `source`.
     *
     * @param source int[] - The array to check in
     * @param target int - The element to check for
     * @return int - The first index of `target` within `source`, otherwise, -1
     *
     * @example firstIndexOf(new int[]{1, 2, 2, 3}, 2);
     * //returns 1
     * @author Angel Kaloyanov
     */

    public static int firstIndexOf(int[] source, int target) {
        int firstTargetAcquired = -1;
        for (int i = 0; i < source.length; i++){
            if (source[i] == target){
                firstTargetAcquired = i;
                break;
            }
        }
        return firstTargetAcquired;
    }


    /**
     * Inserts `element` at index `index` in `source`.
     *
     * @param source int[] - *The array to insert in*
     * @param index  int - *The index to insert at*
     * @param element int - *The element to insert*
     *
     * @return int[] - *A new array with `element` in it*
     *
     * @example insert(new int[] { 1, 2, 4, 5 }, 2, 3);
     *          //returns {1, 2, 3, 4, 5}
     * @author Deyan Stanchev
     */
    public static int[] insert(int[] source, int index, int element) {
        int[] array = new int[source.length + 1];
        for (int i = 0; i < array.length; i++) {
            if (i == index) {
                array[i] = element;
            } else if (i < index) {
                array[i] = source[i];
            } else {
                array[i] = source[i - 1];
            }
        }
        return array;
    }
    /**
     * Checks if index is a valid index in source.
     *
     * @param source int[]  The array of integers to check is index is valid.
     * @param index  int - The index to check for in array.
     * @return boolean - true if index is valid, otherwise, false.
     * @author Kristian Kinchev
     */

    public static boolean isValidIndex(int[] source, int index) {
        for (int i = 0; i < source.length; i++) {
            if (index < source.length && index >= 0) {
                return true;
            }
        }
        return false;

    }
    /**
     * Finds the last index of target within source
     *
     * @param source int[] - The array to check in
     * @param target int - The element to check for
     * @return int - The last index of target within source, if element is not present return -1
     * @author - Nadya Chukleva
     */
    public static int lastIndexOf(int[] source, int target) {
        int targetIndex = -1;
        for (int i = 0; i <= source.length - 1; i++) {
            if (source[i] == target) {
                targetIndex = i;
            }
        }
        return targetIndex;
    }

    /**
     * Removes all occurrences of element within source
     *
     * @param source  int [] - The array to remove from
     * @param element int - The element to check for
     * @return int [] - A new array will all occurrences of element removed
     * @example removeAllOccurrences(new int[]{1, 2, 3, 4, 2}, 2);
     * //return {1, 3, 4}
     * @author Nelly Jekova
     */

    public static int[] removeAllOccurrences(int[] source, int element) {
        int j = 0;
        int count = 0;

        for (int i = 0; i < source.length; i++) {
            if (source[i] == element) {
                count++;
            }
        }
        int[] newArray = new int[source.length - count];

        for (int i = 0; i < source.length; i++) {
            if (source[i] != element) {
                newArray[j] = source[i];
                j++;
            }
        }
        return newArray;
    }

    /**
     * Reverses a given array.
     *
     * @param arrayToReverse The array to reverse
     * @return <code>void</code>
     *
     * @author Angel Gechev
     */
    public static void reverse(int[] arrayToReverse) {
        int [] tempArray = new int[arrayToReverse.length];
        int tempIndex = arrayToReverse.length - 1;
        for (int i = 0; i < tempArray.length; i++) {
            tempArray[tempIndex] = arrayToReverse[i];
            tempIndex--;
        }
        for (int i = 0; i < arrayToReverse.length; i++) {
            arrayToReverse[i] = tempArray [i];
        }
    }




    /**
     * Returns a new array, from `source`, starting from `startIndex` and until `endIndex`
     *
     * @param source int[] - The array to create the new array from
     * @param startIndex int - The starting index
     * @param endIndex int - The end index
     *
     *@return int[] - A new array starting from `startIndex` and until `endIndex`
     *
     * @example section(new int[]{1, 2, 3, 4, 2}, 0, 3)
     * //returns {1, 2, 3}
     *
     * @author AngelKaloyanov
     */



    public static int[] section(int[] source, int startIndex, int endIndex) {

        if (startIndex >= source.length || startIndex > endIndex){
            return source;
        } else if (source.length == 0) {
            return source;
        }


        if (endIndex > source.length){
            endIndex = source.length;
            int newArrLengthCounter = 0;

            for (int i = startIndex; i <source.length; i++){
                newArrLengthCounter++;
            }

            int[] arrSection = new int[newArrLengthCounter]; //Setting up a new array for the section
            for (int i = startIndex; i <source.length; i++) {
                arrSection[i - startIndex] = source[i];
            }
            return  arrSection;
        }

        else {
            int newArrLengthCounter = 0; // We are doing this to calculate the length of the new array - the so called section
            for (int i = startIndex; i <= endIndex; i++){
                newArrLengthCounter++;
            }

            int[] arrSection = new int[newArrLengthCounter]; //Setting up a new array for the section

            for (int i = startIndex; i <= endIndex; i++) {
                arrSection[i - startIndex] = source[i];
            }

            if (startIndex >= source.length || startIndex > endIndex){
                return source;
            }
            else {
                return  arrSection;
            }
        }
    }
}
